OBJS = CMAC.o
CC = g++ -std=c++0x
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
CFLAGS = -std=c++0x
LFLAGS = -Wall $(DEBUG)

cmac : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o cmac

%.o: %.cpp
	$(CC) $(CC_FLAGS) -c -o $@ $<

clean:
	\rm -f *.o cmac *~ 
