/*******************************
* CMAC, cerebellar neuronal model 
* for 2 inputs, 2 outputs
* @author: M.Reithmeier
* @date: dec 2013
********************************/

#include "CMAC.hpp"

#include <iostream>
#include <vector>
#include <cmath>

/*
* C'tor
*/
CMAC::CMAC(unsigned int n_a, const std::vector<unsigned int> displacements, float minX1, float maxX1, float minX2, float maxX2, unsigned int resX1, unsigned int resX2, unsigned int n_w) :
n_a(n_a), displacements(displacements), minX1(minX1), maxX1(maxX1), minX2(minX2), maxX2(maxX2), resX1(resX1), resX2(resX2), n_w(n_w) 
{
     weightY1 = new float[n_w]();
     weightY2 = new float[n_w]();  
}

/*
* D'tor
*/
CMAC::~CMAC()
{
    delete[] weightY1;
    delete[] weightY2;
}


/*
* Input x1, x2 to output y1, y2
*/
std::pair<float,float> CMAC::map(float x1, float x2)
{
    std::vector<unsigned int> au_indexes = quantiziseAndAssociate(x1, x2);


    float y1 = 0;
    float y2 = 0;

    unsigned int idx;

    for(unsigned int i = 0; i < n_a; i++)
    {
        idx = au_indexes[i];
        y1 += weightY1[idx];
        y2 += weightY2[idx];
    }

    return std::make_pair(y1, y2);

}

/*
* Get AU indexes (returns n_a indexes)
*/
std::vector<unsigned int> CMAC::quantiziseAndAssociate(float x1, float x2)
{
    limit(&x1, &x2);

    unsigned int q1 = std::floor( resX1*((x1 - minX1) / (maxX1 - minX1)) );
    unsigned int q2 = std::floor( resX2*((x2 - minX2) / (maxX2 - minX2)) );

    q1 = std::min(q1, resX1-1);
    q2 = std::min(q2, resX2-1);

    std::vector<unsigned int> indexes(n_a);
    unsigned int p1, p2, mu;

    for(unsigned int na = 0; na < n_a; na++)
    {
        p1 = std::floor( (q1+displacements[na]) / n_a );
        p2 = std::floor( (q2+displacements[n_a+na]) / n_a );
        mu = hash(na, p1, p2);

        indexes[na] = mu;
    }

    return indexes;
}

/*
* Input x1, x2 to output y1, y2
*/
void CMAC::train(float x1, float x2, float y1, float y2, float alpha)
{
    auto res = map(x1, x2);

    std::vector<unsigned int> indexes = quantiziseAndAssociate(x1, x2);

    unsigned int idx;
    for(unsigned int na = 0; na < n_a; na++)
    {
        idx = indexes[na];
        weightY1[idx] += alpha*( (y1-res.first) / n_a);
        weightY2[idx] += alpha*( (y2-res.second) / n_a);

    }
    std::cout << "err: " << y1-res.first << "," << y2-res.second << std::endl;
}

/*
* Hash to weight idx
*/
unsigned int CMAC::hash(unsigned int na, unsigned int p1, unsigned int p2)
{
    unsigned int h = 0;
    unsigned int rkX1, rkX2;
        
    rkX1 = std::floor( (resX1 - 2) / n_a) + 2;
    rkX2 = std::floor( (resX2 - 2) / n_a) + 2;

    h = h*rkX1 + p1;
    h = h*rkX2 + p2;

    return h;
}

/*
* Simple range limiting 
*/
void CMAC::limit(float* x1, float* x2)
{
    if(*x1 < minX1)
        *x1 = minX1;
    else if(*x1 > maxX1)
        *x1 = maxX1;

    if(*x2 < minX2)
        *x2 = minX2;
    else if(*x2 > maxX2)
        *x2 = maxX2;
}

int main()
{
    // displacements: offsets for each AU * output size
    const std::vector<unsigned int> displacements = {2, 3, 4, 0, 1,   3, 1, 4, 2, 0};

    //n_a, displacements, minX1, maxX1,minX2, maxX2, resX1, resX2,  n_w
    CMAC cmac(5, displacements, 0, 1, 0, 1, 50, 50, 2048);
    
    std::cout << "Training" << std::endl;

    for (int i = -50; i < 50 ; i++) {
        cmac.train(0.3+(i*0.005), 0.5, 20, 1, 0.4);
    }


    
    std::cout << "Output" << std::endl;
    auto test = cmac.map(0.3, 0.5);
    std::cout << test.first << "|" << test.second << std::endl;

}




