#ifndef _CMACH_
#define _CMACH_

#include <vector>

/*
* CMAC for 2 to 2 dimensions
*/
class CMAC
{

  public:
    
    /*
    * C'tor
    */
    CMAC(unsigned int n_a, const std::vector<unsigned int> displacements, float minX1, float maxX1, float minX2, float maxX2, unsigned int resX1, unsigned int resX2, unsigned int n_w);

    /*
    * D'tor
    */
    ~CMAC();

    /*
    * Input x1, x2 to output y1, y2
    */
    std::pair<float,float> map(float x1, float x2);
    
    /*
    * Input x1, x2 to output y1, y2
    */
    void train(float x1, float x2, float y1, float y2, float alpha);


  private:

    unsigned int n_a; // number of AUs
    unsigned int n_w; // numbe of weights per output;
    unsigned int resX1, resX2; // quantisation resolution for both inputs
    std::vector<unsigned int> displacements; // displacement offset for each AU (here: same for each input)
    float minX1, maxX1, minX2, maxX2; // input limits

    float* weightY1;
    float* weightY2;

    /*
    * Simple range limiting 
    */
    void limit(float* x1, float* x2);

    /*
    * Hash to weight idx
    */
    unsigned int hash(unsigned int na, unsigned int p1, unsigned int p2);

    /*
    * Get AU indexes (returns n_a indexes)
    */
    std::vector<unsigned int> quantiziseAndAssociate(float x1, float x2);

};

#endif
